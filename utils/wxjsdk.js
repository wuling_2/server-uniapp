import wxjssdk from 'weixin-js-sdk'
	import {showToast, showSuccessToast, showFailToast } from 'vant';
	export default {
		
		async getLocation() {
			var url = location.href.split("#")[0]; // 获取当前页面的url中#前面那部分
			var appId = "wxe489380fa569e3f3"; // 公众号的唯一标识appid，注意大小写  一鲸：wxe489380fa569e3f3   我的：wxccf4a1e7f008f43e
			var getSignatureUrl = "https://wx.wulings.com/wx.php"; //你们后端写好的签名的获取接口
			var getSignatureConfig = { getSignatureUrl }; //你们后端写好的签名的获取接口
			let getSignatureResponse = await uni.request({
				url: getSignatureUrl,
				method: 'GET', // 请求方法，可选值包括 'GET'、'POST' 等
				data: {url: window.location.href}, // 请求参数
				header: {
					'content-type': 'application/json' // 设置请求的 header，根据实际需要设置
				},
				success: res => {
					// 请求成功时的回调函数
					var nonceStr = res.data.nonceStr; // 生成签名的随机串,注意这个s是小写
					var timestamp = res.data.timestamp; // 生成签名的时间戳,到秒级
					var signature = res.data.signature; // 生成签名的时间戳,到秒级
					// var config = { nonceStr, timestamp, url }; //获取微信签名接口所需要的参数
					var jsApiList = ['getLocation']; //需要使用的微信JS接口列表
					let wxconfig = {
						debug: false, // 开启调试模式,调用的所有api的返回值会在客户端alert出来，若要查看传入的参数，可以在pc端打开，参数信息会通过log打出，仅在pc端时才会打印。
							appId: appId, // 必填，公众号的唯一标识
							timestamp: res.data.timestamp+"", // 必填，生成签名的时间戳
							nonceStr: nonceStr, // 必填，生成签名的随机串
							signature: signature,// 必填，签名
							jsApiList: jsApiList// 必填，需要使用的JS接口列表
					};
					// console.log("wxconfig", wxconfig);
					// console.log('ready2',wxconfig);
					wxjssdk.config(wxconfig);
					wxjssdk.ready(() => {
						wxjssdk.getLocation({
							// 默认为wgs84的gps坐标，如果要返回直接给openLocation用的火星坐标，可传入'gcj02'
						  type: 'wgs84', 
						  // type: 'gcj02', 
						  success: function (res) {
							uni.setStorageSync('latitude', res.latitude); // 纬度，浮点数，范围为90 ~ -90
							uni.setStorageSync('longitude', res.longitude); // 经度，浮点数，范围为180 ~ -180。
							// console.log("success",res);
						  },
						  fail: function (res) {
							// console.log("fail",res)
							// showToast("请同意获取位置信息，否则将会影响后续操作")
						  },
						  complete: function (res) {
							// console.log("complete",res)
						  }
						});
					});
				},
				fail: err => {
					// 请求失败时的回调函数
					console.log(err);
				}
			}); 
		},
		async scanQRCode({ success, fail, needResult }) {
			var url = location.href.split("#")[0]; // 获取当前页面的url中#前面那部分
			var appId = "wxe489380fa569e3f3"; // 公众号的唯一标识appid，注意大小写  一鲸：wxe489380fa569e3f3   我的：wxccf4a1e7f008f43e
			var getSignatureUrl = "https://wx.wulings.com/wx.php"; //你们后端写好的签名的获取接口
			var getSignatureConfig = { getSignatureUrl }; //你们后端写好的签名的获取接口
			let getSignatureResponse = await uni.request({
				url: getSignatureUrl,
				method: 'GET', // 请求方法，可选值包括 'GET'、'POST' 等
				data: {url: window.location.href}, // 请求参数
				header: {
					'content-type': 'application/json' // 设置请求的 header，根据实际需要设置
				},
				success: res => {
					// 请求成功时的回调函数
					var nonceStr = res.data.nonceStr; // 生成签名的随机串,注意这个s是小写
					var timestamp = res.data.timestamp; // 生成签名的时间戳,到秒级
					var signature = res.data.signature; // 生成签名的时间戳,到秒级
					// var config = { nonceStr, timestamp, url }; //获取微信签名接口所需要的参数
					var jsApiList = ['scanQRCode']; //需要使用的微信JS接口列表
					let wxconfig = {
						debug: false, // 开启调试模式,调用的所有api的返回值会在客户端alert出来，若要查看传入的参数，可以在pc端打开，参数信息会通过log打出，仅在pc端时才会打印。
							appId: appId, // 必填，公众号的唯一标识
							timestamp: res.data.timestamp+"", // 必填，生成签名的时间戳
							nonceStr: nonceStr, // 必填，生成签名的随机串
							signature: signature,// 必填，签名
							jsApiList: jsApiList// 必填，需要使用的JS接口列表
					};
					// console.log("wxconfig", wxconfig);
					// console.log('ready2',wxconfig);
					wxjssdk.config(wxconfig);
					wxjssdk.ready(() => {
						wxjssdk.scanQRCode({
							needResult, // 默认为0，扫描结果由微信处理，1则直接返回扫描结果，
							// scanType: ["qrCode", "barCode"], // 可以指定扫二维码还是一维码，默认二者都有
							success: res => {
								// {errMsg: "scanQRCode:ok", resultStr: ""} 当needResult 为 1 时，resultStr扫码返回的结果
								// {errMsg: "scanQRCode:ok"}当needResult 为 0 时，会导致页面跳转
								//扫码成功，抛出扫码结果
								success(res);
							},
						});
					});
				},
				fail: err => {
					// 请求失败时的回调函数
					console.log(err);
				}
			}); 
		},
	};