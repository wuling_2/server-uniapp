module.exports = {
  devServer: {
    proxy: {
      '/Api': {
        target: 'https://api.weixin.qq.com', // 填写你需要代理的请求的后端接口地址
        changeOrigin: true,
        pathRewrite: {
          '^/Api': '' // 如果后端接口不包含 '/api' 前缀，可以将其替换为空字符串或其他前缀
        }
      }
    }
  }
}